package DynamicElement;

public class CallByValAndRef {

	int r = 10;

	void change(int r) {
		r = r + 10;

	}

	void change1(CallByValAndRef ob) {
		int r = 0;
		ob.r = ob.r + 10;

	}

	public static void main(String[] args) {

		CallByValAndRef obj = new CallByValAndRef();
		System.out.println("before" + obj.r);

//		obj.change(10);
//		System.out.println("After" + obj.r);
		obj.change1(obj);
		System.out.println("After" + obj.r);

	}

}


public class ConstructorInJava {

	int i;	
	public ConstructorInJava() {
		System.out.println("in default constructor");
	}	
	
	public ConstructorInJava(int a) {
		i=a;
	}
//	public void set(int a) {
//	  i=a;
//	}	
	public void show() {
		printTable();
		System.out.println(i);
	}	
	
	
	public void printTable() {
		for(int b=1;b<=10;b++) {
			System.out.println(i*b);			
		}
	}	
	public static void main(String[] args) {
//		ConstructorInJava obj = new ConstructorInJava();
		ConstructorInJava obj1 = new ConstructorInJava(12);
		obj1.show();
		obj1.printTable();		
	}

}

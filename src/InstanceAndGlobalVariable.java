
public class InstanceAndGlobalVariable {

	int a=12;     // instance variable  
	

	public void show() {
		
		int a=29;   //local variable
		a=32;
		System.out.println(a);
	}
	
	public void print() {
		System.out.println(a);
	}
	
	public static void main(String[] args) {
		
		InstanceAndGlobalVariable obj = new InstanceAndGlobalVariable();
		obj.show();
		obj.print();

	}

}

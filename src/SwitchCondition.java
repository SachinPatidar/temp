
public class SwitchCondition {

	public static void main(String[] args) {
		
		int a=1;
		
		switch(a) {
		case 1:
			System.out.println("jan");
			break;
		case 2:
			System.out.println("feb");
			break;
		default:
			System.out.println("no match");
		}
		
	}

}

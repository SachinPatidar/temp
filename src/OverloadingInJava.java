
public class OverloadingInJava {
	
	public OverloadingInJava() {
		System.out.println("in constructor");
	}
	public OverloadingInJava(int i) {
		System.out.println("in parametrized constructor");
	}
	public void add(int a,int b ) {
		System.out.println( a+b);	
	}		
	public void add(int a,int b,int c) {
		System.out.println( a+b+c);		
	}	
	public void add(float a,int b ) {
		System.out.println( a+b);	
	}
	
	public static void main(String[] args) {
		OverloadingInJava obj = new OverloadingInJava();
		obj.add(12, 13);
		obj.add(12.2f, 13);
	}

}

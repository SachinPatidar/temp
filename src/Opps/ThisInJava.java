package Opps;

public class ThisInJava {
	
	int a=10;  // instance variable
	
	public ThisInJava(){
		
		System.out.println("in con");
	}

	public ThisInJava(int a){
		this();
		System.out.println("in para con");
	}
	
	public int add(int a) {
//		this.a=a;
		System.out.println(a);
		System.out.println(this.a);
		
		return 2;
	}
	
	public void showw() {
		this.add(90);
		System.out.println(a);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		ThisInJava obj ;
//		obj= new ThisInJava();
//		obj.add(23);
//		obj.showw();
//		
		ThisInJava obj1 = new ThisInJava(23);
		
	}

}

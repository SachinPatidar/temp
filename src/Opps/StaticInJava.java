package Opps;

public class StaticInJava {

	int rollNo;
	static String college= "LNCT";
	int temp=0;
	
	static {
		System.out.println("before Main");
		System.exit(0);
	}
	
	
	public static void print() {
		System.out.println("in print");
	}
	
	
	public void set(int r) {
		rollNo= r;
	}
	
	public void show() {
		System.out.println(rollNo);
		System.out.println(college);
		System.out.println(temp);
	}
	
	public void teee() {
		temp++;
	}
	
	
	public static void main(String[] args) {
		System.out.println("in main");
		StaticInJava st1 = new StaticInJava() ;
		
		st1.set(102);
		st1.show();
		StaticInJava.college="oriental";
		
		StaticInJava st2 = new StaticInJava() ;
		st2.set(105);
		st2.show();
		StaticInJava st3 = new StaticInJava() ;
		
		
		StaticInJava.print();
		
	}

}
